"""
Slang - Parser
By Katelyn Hamer
"""

from rply import ParserGenerator
from ast import Out, Number, Sub, Add


class Parser:
    def __init__(self):
        self.pg = ParserGenerator(
            # A list of all token names accepted by the parser.
            ["NUMBER", "OPEN_PAREN", "CLOSE_PAREN", "OUT", "ADD", "SUB"]
        )

    def parse(self):
        @self.pg.production("program : OUT OPEN_PAREN expression CLOSE_PAREN")
        def program(p):
            return Out(p[2])

        @self.pg.production("expression : expression SUB expression")
        @self.pg.production("expression : expression ADD expression")
        def expression(p):
            left = p[0]
            right = p[2]
            op = p[1]
            if op.gettokentype() == "SUB":
                return Sub(left, right)
            elif op.gettokentype() == "ADD":
                return Add(left, right)

        @self.pg.production("expression : NUMBER")
        def number(p):
            return Number(p[0].value)

        @self.pg.error
        def error_handle(token):
            raise ValueError(token)

    def get_parser(self):
        return self.pg.build()
