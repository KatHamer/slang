#!/usr/bin/env python3
"""
Slang
A simple programming language written in Python
By Katelyn Hamer
"""

from lexer import Lexer
from parser import Parser
import click
import rply
import sys
import pathlib

LEXER = Lexer().get_lexer()

if len(sys.argv) > 1:
    file_to_run = sys.argv[1]
    file_path = pathlib.Path(file_to_run)
    if not file_path.exists():
        print(f"{file_path.resolve()}: no such file or directory.")
        exit(1)
else:
    print("Usage: ./__main__.py <file-to-run>.sl")
    exit(2)

with open(file_path) as fp:
    program = fp.read()
    print(program)

tokens = LEXER.lex(program)

"""    
try:
    for token in tokens:
        click.secho(click.style("Valid token! ", fg="green") + f"{token=}")
except rply.errors.LexingError as lexing_error:
    
    click.secho(click.style(f"Lexer error! ", fg="red") + f"{lexing_error=}")
"""

pg = Parser()
pg.parse()
parser = pg.get_parser()
parsed_tokens = parser.parse(tokens)
parsed_tokens.eval()  # pylint: disable=no-member
