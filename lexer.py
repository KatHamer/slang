"""
Slang - Lexer
By Katelyn Hamer
"""

from rply import LexerGenerator


class Lexer:
    def __init__(self):
        self.lexer = LexerGenerator()

    def _add_tokens(self):
        # self.lexer.add("COMMENT", r'! .*')
        self.lexer.add("NUMBER", r"\d+")
        self.lexer.add("OPEN_PAREN", r"\(")
        self.lexer.add("CLOSE_PAREN", r"\)")
        self.lexer.add("QUOTE", r"\"")
        self.lexer.add("OUT", r"out")
        self.lexer.add("ADD", r"\+")
        self.lexer.add("SUB", r"\-")
        # self.lexer.add("ANY", r'.*')
        # self.lexer.add("WHITESPACE", r'\s+')
        self.lexer.ignore(r"\s+")
        self.lexer.ignore("! .*")

    def get_lexer(self):
        self._add_tokens()
        return self.lexer.build()
