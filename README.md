# [WIP] Slang

A simple programming language developed to teach myself programming language development. The code is based off of [this excellent tutorial](https://blog.usejournal.com/writing-your-own-programming-language-and-compiler-with-python-a468970ae6df) by [Marcelo Andrade](https://medium.com/@marcelogdeandrade).

## What works:

- Comments: `! This is a comment`
- Printing numbers: `out(5)`
- Simple addition/subtraction: `out(9+10)`

## What doesn't work:

- Everything else.
- Using the out statement multiple times results in a token error for some reason.
- There are multiple shift/reduce conflicts. I'm not 100% sure what these are yet or how to resolve them.
- Compiling binaries using llvmlite.

## How to test

- Clone the repo: `$ git clone https://gitlab.com/kathamer/slang`
- Create a virtual environment: `$ python3 -m venv venv`
- Activate the virtual environment: `$ source venv/bin/activate` 
- Install the requirements" `$ pip install -r requirements.txt`
- Run the sample Slang file: `./__main__.py hello_world.sl`