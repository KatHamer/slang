"""
Slang - AST
By Katelyn Hamer
"""


class Out:
    def __init__(self, value):
        self.value = value

    def eval(self):
        print(self.value.eval())


class BinaryOp:
    def __init__(self, left, right):
        self.left = left
        self.right = right


class Sub(BinaryOp):
    def eval(self):
        return self.left.eval() - self.left.eval()


class Add(BinaryOp):
    def eval(self):
        return self.left.eval() + self.right.eval()


class Number:
    def __init__(self, value):
        self.value = value

    def eval(self):
        return int(self.value)
